let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    let array = []
    for (i = 0; i < collection.length-1; i++){
        array += array[i]
    }
    return array
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    collection[collection.length] = element;
    return collection
}

function dequeue() {
    // In here you are going to remove the FIRST element in the array
    let firstArr = []
    collection.splice(0,1)
    firstArr = [collection[0]]
    return firstArr
}

function front() {
    // In here, you are going to remove the first element
        return collection[0];
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements 
     let length = 0;
     while(collection[length] !== undefined){
            length++;
     }
     return length
}

function isEmpty() {
    //it will check whether the function is empty or not
    if(collection.length==0){
        return true
    }
    else{
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};